function commence(){
    zoneData=document.getElementById("zoneData");
    var monBouton = document.getElementById("monBouton");
    monBouton.addEventListener("click",lire, false);
}

function lire(){
    var monurl = "text.txt";
    var demande = new XMLHttpRequest();
    demande.addEventListener("load", montrer, false);
    demande.open("GET", monurl, true);
    demande.send(null);
}

function montrer(e){
    zoneData.innerHTML = e.target.responseText;
}

window.addEventListener("load", commence, false);